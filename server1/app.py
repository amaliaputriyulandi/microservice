from crypt import methods
from urllib import response
from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
import requests

# Inisiasi object
app = Flask(__name__)

# inisiasi object flask_restful
api = Api(app)

# Inisiasi object flask_cors
CORS(app)

#class rajaongkir
class RajaOngkir(Resource):
    def get(self):
        # tentukan url endpoint
        
        url = "https://api.rajaongkir.com/starter/cost"

        headers = {
            "key": "36b48af84271a156c127fc3132400703",
            'content-type': "application/x-www-form-urlencoded"
                    }
        payload = "origin=501&destination=114&weight=1700&courier=jne"
        # lakukan http request ke server
        response = requests.post(url, payload, headers=headers)
        return response.json()

# setup resourcenya
api.add_resource(RajaOngkir, "/api/rajaongkir", methods=["GET"])

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=5005)