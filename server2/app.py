from flask import Flask, request, jsonify, make_response
import pymysql
# Membuat server flask
app = Flask(__name__)

# Koneksi ke database MySQL
mydb = pymysql.connect(
	host="localhost", #kalau di cloud biasanya di link yang berjalan
	user="debian-sys-maint",
	passwd="4McmT4XR59tBOr8J",
	database="db_sekolah"
)

# Routing index
@app.route('/')
@app.route('/index')
def index():
	return "Hello, World!"

@app.route('/get_data_siswa', methods=['GET'])
def get_data_siswa():
	query = "SELECT * FROM tb_siswa WHERE 1=1"
	values = ()

	nis = request.args.get("nis")
	nama = request.args.get("nama")
	umur = request.args.get("umur")

	if nis: # apakah ada data nis
		query += " AND nis=%s " # placeholder? 
		values += (nis,)
	if nama:
		query += " AND nama LIKE %s " 
		values += ("%"+nama+"%", )
	if umur:
		query += " AND umur=%s "
		values += (umur,)

	mycursor = mydb.cursor()
	mycursor.execute(query, values)	 # di excute dilihat lagi query nya 
	row_headers = [x[0] for x in mycursor.description]
	data = mycursor.fetchall() #kita ambil datanya kita masukkan ke dalam variabel data bentuknya masih list bukan json
	json_data = []
	for result in data: # dari data di ubah kan ke json
		json_data.append(dict(zip(row_headers, result))) # nnti append ke json_data dalam bentuk dictionary
	return make_response(jsonify(json_data),200) # sebenarnya pakai return boleh tapi dgn menggunakan make_response memudahkan pemberian status 200
	#jsonify dicheck apakah nilai nya string json jika bukan akan dibuat json

@app.route('/insert_data_siswa', methods=['POST'])
def insert_data_siswa():
	hasil = {"status": "gagal insert data siswa"}
	
	try:
		data = request.json # menerima data json dan disimpan ke variabel data

		query = "INSERT INTO tb_siswa(nis, nama, umur, alamat) VALUES(%s,%s,%s,%s)" #ada 4 placeholders dari data-data itu tidak boleh lansung di isi agar tidak injection
		values = (data["nis"], data["nama"], data["umur"], data["alamat"],)
		mycursor = mydb.cursor() # kita cursor
		mycursor.execute(query, values) # kita execute
		mydb.commit() # jika kita tambah data, delete, update data harus commit
		hasil = {"status": "berhasil insert data siswa"}

	except Exception as e: # fungsi try except agar mengetahui jika error akan masuk ke except
		print("Error: " + str(e))
		hasil = { #ini tambahan coba aja
			"status": "gagal insert data siswa",
			"error" : str(e)
		}

	return jsonify(hasil)

@app.route('/update_data_siswa', methods=['PUT'])
def update_data_siswa():
	hasil = {"status": "gagal update data siswa"}
	
	try:
		data = request.json # menerima data didalam body nya
		nis_awal = data["nis_awal"]

		query = "UPDATE tb_siswa SET nis = %s " # set nis = %s agar kedepannya dapat diubah
		values = (nis_awal, ) # bentuk nya tuple dan dianggap array dengan 1 data diakhiri 

		#untuk values akhirnya array

		if "nis_ubah" in data:
			query += ", nis = %s" # apakah FE mengirimkan ubah nama kalau iya ubah datanya
			values += (data["nis_ubah"], )
		if "nama" in data:
			query += ", nama = %s"
			values += (data["nama"], )
		if "umur" in data:
			query += ", umur = %s"
			values += (data["umur"], )
		if "alamat" in data:
			query += ", alamat = %s"
			values += (data["alamat"], )

		query += " WHERE nis = %s"
		values += (nis_awal, )

		mycursor = mydb.cursor()
		mycursor.execute(query, values)
		mydb.commit()
		hasil = {"status": "berhasil update data siswa"}

	except Exception as e:
		print("Error: " + str(e))

	return jsonify(hasil)

@app.route('/delete_data_siswa/<nis>', methods=['DELETE']) # <> ini adalah sebuah data yang diterima sesuai fungsi nya
def delete_data_siswa(nis):
	hasil = {"status": "gagal hapus data siswa"}
	
	try:
		# data = request.json

		query = "DELETE FROM tb_siswa WHERE nis=%s" # %s placeholders ya
		values = (nis,)
		mycursor = mydb.cursor() # untuk membuat cursor ke mysql, kita ga bisa langsung execute ke mysql kita harus buat cursor.
		delete_data = mycursor.execute(query, values)
		if delete_data == 0:
			return jsonify({
				"msg": "data nis tidak ditemukan!",
				"status": 400
			})
		mydb.commit()
		hasil = {"status": "berhasil hapus data siswa"}

	except Exception as e:
		print("Error: " + str(e))

	return jsonify(hasil)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5010, debug=True) #host nya bisa di hapus dan setiap backend kalo bisa beda-beda
	# debug=True hanya boleh dijalankan saat tahap development
