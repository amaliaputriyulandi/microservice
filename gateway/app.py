from flask import Flask, request, jsonify, make_response
from urllib import response, request
import requests
app = Flask(__name__)

# Routing index
@app.route('/')
@app.route('/index')
def index():
	return "Hello, World!"

#server 1
@app.route('/get_server1', methods=['GET'])
def get_server1():
	
	url = "http://94.74.127.32:5005/api/rajaongkir"
	
	response = requests.get(url)
	
	return response.json()

#server 2
@app.route('/get_server2', methods=['GET'])
def get_server2():
	url = "http://94.74.127.32:5010/get_data_siswa"
	
	response = requests.get(url)
	hasil = response.json()
	return jsonify(hasil)
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)